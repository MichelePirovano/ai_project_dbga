﻿using System.Collections;
using System.Collections.Generic;
using AI.Decision;
using AI.Steering;
using UnityEngine;

namespace AI.Decision.DecisionTree
{
	public abstract class Node
	{
		public Brain myBrain;
		
		public abstract void MakeDecision();
	}

	public abstract class Decision : Node
	{
		public Node trueNode;
		public Node falseNode;

		public abstract bool CheckCondition();
		
		public override void MakeDecision()
		{
			if (CheckCondition())
			{
				trueNode.MakeDecision();
			}
			else
			{
				falseNode.MakeDecision();
			}
		}
	}

	public abstract class Action : Node
	{
		public override void MakeDecision()
		{
			DoAction();
		}

		public abstract void DoAction();
	}
	
	
	public class DecisionTree_DecisionMaker : DecisionMaker
	{
		public override void MakeDecision()
		{
			// Reset action state
			GetComponent<Shooter>().StopShooting();
			GetComponent<Agent>().maximumLinearVelocity = 5;
			GetComponent<SeekBehaviour>().weight = 0;
			GetComponent<FleeBehaviour>().weight = 0;
			
			// Create the decision tree
			var hlCond = CreateNode<HealthLowCondition>();
			var ecCond = CreateNode<EnemyCloseCondition>();

			hlCond.trueNode = CreateNode<FleeBase>();
			hlCond.falseNode = ecCond;
			
			ecCond.trueNode = CreateNode<ShootEnemy>();
			ecCond.falseNode = CreateNode<BossOrder>();	// Objective from boss 
			
			var root = hlCond;
			
			// Call MakeDecision on the root node
			root.MakeDecision();
		}

		T CreateNode<T>() where T : Node, new()
		{
			var node = new T();
			node.myBrain = GetComponent<Brain>();
			return node;
		}
		
	}
    
	
	
	
	
	public class TacticianDecisionTree_DecisionMaker : DecisionMaker
	{
		public override void MakeDecision()
		{
			// Create the decision tree
			//var tooManyEnemies = CreateNode<TooManyEnemiesCondition>();
			//tooManyEnemies.trueNode = CreateNode<GiveOrder0>();
			//tooManyEnemies.falseNode = CreateNode<GiveOrder1>();
			
			// Call MakeDecision on the root node
			//tooManyEnemies.MakeDecision();
		}

		T CreateNode<T>() where T : Node, new()
		{
			var node = new T();
			node.myBrain = GetComponent<Brain>();
			return node;
		}
		
	}

}
