﻿using System.Collections;
using System.Collections.Generic;
using AI.Pathfinding;
using UnityEngine;

public class DijkstraSearch : MonoBehaviour
{
    public GraphMaker graphMaker;

    public float step = 0.2f;
    
    public List<Edge> path = new List<Edge>();
    
    void Start()
    {
        StartCoroutine(Search());
    }

    public IEnumerator Search()
    {
        List<Node> closedSet = new List<Node>();    // List of visited nodes
        List<Node> openSet = new List<Node>();    // List of nodes to visit
        openSet.Add(graphMaker.startNode);
        graphMaker.startNode.color = Color.yellow;

        // Initialise all nodes with distance infinity, start node has distance 0
        foreach (var node in graphMaker.nodes)
        {
            node.value = Mathf.Infinity;
        }
        graphMaker.startNode.value = 0;

        bool pathFound = false;
        while (openSet.Count > 0)
        {
            // Find the node with the minimum value
            Node current = null;
            var minValue = Mathf.Infinity;
            foreach (var openNode in openSet)
            {
                if (openNode.value < minValue)
                {
                    minValue = openNode.value;
                    current = openNode;
                }
            }    
            //current.color = Color.red;
            current.color = Color.Lerp(Color.yellow, Color.red, current.value / 200f);
            openSet.Remove(current);
            closedSet.Add(current);
            
            if (current == graphMaker.endNode)
            {
                pathFound = true;
                break;
            }

            var neighbours = graphMaker.GetNeighbours(current);
            foreach (var neighbour in neighbours)
            {
                // Add the node to the open set, if it is a new node
                if (!closedSet.Contains(neighbour)
                    && !openSet.Contains(neighbour))
                {
                    openSet.Add(neighbour);
                    neighbour.color = Color.Lerp(Color.yellow, Color.red, neighbour.value / 200f); //Color.yellow;
                }

                // We compute the cost of the segment
                var segmentCost = Vector2.Distance(current.transform.position,
                    neighbour.transform.position);

                var newValue = current.value + segmentCost;
                if (newValue < neighbour.value)
                {
                    neighbour.value = newValue;
                }
            }

            yield return new WaitForSeconds(step);
        }

        if (pathFound)
        {
            Debug.Log("Found a path!");

            Node currentNode = graphMaker.endNode;
            while (currentNode != graphMaker.startNode)
            {
                // Find the neighbour that moves us to the shortest path
                var neighbours = graphMaker.GetNeighbours(currentNode);
                Node bestNeighbour = null;
                var minValue = Mathf.Infinity;
                foreach (var neighbour in neighbours)
                {
                    var segmentCost = Vector2.Distance(currentNode.transform.position,
                        neighbour.transform.position);

                    var totalCost = neighbour.value + segmentCost;
                    if (totalCost < minValue)
                    {
                        minValue = totalCost;
                        bestNeighbour = neighbour;
                    }
                }

                // Find the edge between the two nodes and add it to the tha path
                foreach (var edge in graphMaker.edges)
                {
                    if ((edge.nodeA == currentNode && edge.nodeB == bestNeighbour)
                        || (edge.nodeB == currentNode && edge.nodeA == bestNeighbour))
                    {
                        path.Add(edge);
                        break;
                    }
                }

                // Move to the next neighbour
                currentNode = bestNeighbour;
            }
            
            // Reverse the order of the path to find the path from Ns to Ne
            path.Reverse();
            for (var i = 0; i < path.Count; i++)
            {
                var edge = path[i];
                edge.color = Color.Lerp(Color.green, Color.yellow, (float)i/ path.Count);
            }
        }
        else
        {
            Debug.Log("No path found!");
        }
    }

}
