﻿using System.Collections;
using System.Collections.Generic;
using AI.Pathfinding;
using UnityEngine;

namespace AI.Steering
{
	public class FollowPathBehaviour : SteeringBehaviour
	{
		public DijkstraSearch pathfinder;
		public Node currentNode;
		public int currentPathIndex;

		public Transform targetTransform;

		public float arrivalThreshold = 1f;
		
		public override SteeringOutput GetSteering()
		{
			// While the path is not found, wait.
			SteeringOutput steering = new SteeringOutput();
			if (pathfinder.path.Count == 0)
			{
				steering.inactive = true;
				return steering;
			}

			// Initialise at the first node
			if (currentNode == null)
			{
				currentPathIndex = 0;
				currentNode = pathfinder.graphMaker.startNode;
				currentNode.color = Color.blue;
			}

			// Get the next node from the current path
			Node nextNode = null;
			var edge = pathfinder.path[currentPathIndex];
			if (edge.nodeA == currentNode)
			{
				nextNode = edge.nodeB;
			}
			else if (edge.nodeB == currentNode)
			{
				nextNode = edge.nodeA;
			}
			nextNode.color = Color.cyan;
			targetTransform = nextNode.transform;
			
			// Follow the transform
			if (targetTransform == null) return steering;
			var direction = (targetTransform.position - transform.position);
			direction.z = 0;
			steering.targetLinearVelocityPercent = direction.normalized;

			// If we arrived at the current node, go to the next
			if (Vector2.SqrMagnitude((Vector2) transform.position - (Vector2) targetTransform.position) < arrivalThreshold*arrivalThreshold)
			{
				// Go to the next node, if we did not end
				if (currentPathIndex < pathfinder.path.Count)
				{
					currentPathIndex++;
					currentNode = nextNode;
				}
			}
			return steering;
		}
	}

}