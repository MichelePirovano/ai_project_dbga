﻿using UnityEngine;

namespace AI.PCG.Dungeon
{
	public class DungeonCell : MonoBehaviour
	{
		public int i;
		public int j;
		public bool isWall;

		public void SetWall()
		{
			isWall = true;
			GetComponent<MeshRenderer>().material.color = new Color(1f, 0.6f, 0f, 1f);
		}

		public void SetFloor()
		{
			isWall = false;
			GetComponent<MeshRenderer>().material.color = new Color(0f, 0.2f, 0.2f, 1f);
		}

		public void SetTarget()
		{
			GetComponent<MeshRenderer>().material.color = Color.yellow;
		}
		
	}
}