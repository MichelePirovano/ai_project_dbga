﻿namespace AI.Decision.GOAP
{
	public class Action_ChopTree : Action
	{
		protected override void Awake()
		{
			base.Awake();
			preconditions["durability"] = 1; // >=
			effects["durability"] = -1;
			effects["wood"] = +4;
		}

		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
			agent.axeDurability += effects["durability"];
		}
	}
}