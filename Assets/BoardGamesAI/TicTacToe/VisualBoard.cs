﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AI.TicTacToe
{
    // TODO: also use color to color cells as the algorithm goes on
    public class VisualBoard : MonoBehaviour
    {
        private VisualCell[] cells;

        public VisualCell currentPlayer;
        public Text stateValue;
        
        void Awake()
        {
            cells = GetComponentsInChildren<VisualCell>();
        }
        
        public void ShowState(State state)
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    int index = i * 3 + j;
                    cells[index].SetValue(state.cells[i, j]);
                }
            }

            currentPlayer.SetValue(state.player);
        }
        
    }

}
