﻿using System.Collections.Generic;
using UnityEngine;

namespace AI.Pathfinding
{
	public class Edge : MonoBehaviour
	{
		public Node nodeA;
		public Node nodeB;

		public override bool Equals(object other)
		{
			var other_edge = (Edge) other;
			if (other_edge == null) return false;

			return (nodeA == other_edge.nodeA && nodeB == other_edge.nodeB)
			       ||
			       (nodeA == other_edge.nodeB && nodeB == other_edge.nodeA);
		}
		
		public Color color = Color.white;

		public void Draw()
		{
			Debug.DrawLine(nodeA.transform.position, nodeB.transform.position, color);
		}
	}
}