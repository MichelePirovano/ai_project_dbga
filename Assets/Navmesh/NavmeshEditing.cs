﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using Random = System.Random;

public class NavmeshEditing : MonoBehaviour
{
    public Transform rootTransform;
    public NavMeshAgent agent;
    
    public Vector3 from;
    public Vector3 to;
    
    void Start()
    {
        // Use pathfinder with navmesh without NavMeshAgent
        var filters = new NavMeshQueryFilter();
        filters.agentTypeID = 0;
        filters.areaMask = NavMesh.AllAreas;
        var path = new NavMeshPath();
        NavMesh.CalculatePath(from, to, filters, path);
       
        // Update at runtime the area cost of the given area
        NavMesh.SetAreaCost(5, 0);
        
        // Use Agents without pathfinder
        //agent.velocity = Vector3.one * 10;
        //NavMeshPath newPath = new NavMeshPath();
        var newPath = new NavMeshPath();
        agent.CalculatePath(to, newPath);
        for (int i = 0; i < newPath.corners.Length; i++)
        {
            newPath.corners[i].x += 20f * UnityEngine.Random.value;
        }
        for (int i = 0; i < newPath.corners.Length-1; i++)
        {
            Debug.DrawLine(newPath.corners[i], newPath.corners[i + 1], Color.red, 10f);
        }
        agent.ResetPath();
        agent.SetPath(newPath);    // This will do nothing :( 
        
        // Extract the polygons representing the navmesh
        NavMeshTriangulation navMesh = NavMesh.CalculateTriangulation();
        Vector3[] vertices = navMesh.vertices;
        //int[] polygons = navMesh.indices;    // Mesh triangles
        for (int i = 0; i < vertices.Length-1; i++)
        {
            Debug.DrawLine(vertices[i], vertices[i + 1], Color.yellow, 10f);
        }
    }

}
