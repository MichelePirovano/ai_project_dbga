﻿using System.Collections.Generic;

namespace AI.Decision.DecisionTree
{
	public class BossOrder : Action
	{
		private List<Action> possibleOrders;
		
		public override void DoAction()
		{
			possibleOrders = new List<Action>
			{
				new SeekBase(), // Go to base
				new FleeBase() // Go away
				// ..... other orders, such as... caputer the flag, defend, attack
			};

			int givenOrder = myBrain.currentOrder;
			possibleOrders[givenOrder].myBrain = myBrain;
			possibleOrders[givenOrder].DoAction();
		}
	}
}