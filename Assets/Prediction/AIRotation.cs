﻿using System.Collections;

namespace AI.NGrams
{
	public class AIRotation : Player
	{
		public Action StartingAction;
		
		public override IEnumerator GetAction(Answer answer)
		{
			var actionToReturn = StartingAction;
			StartingAction += 1;
			if (StartingAction == (Action) 4) StartingAction = (Action) 1;
			answer.action = actionToReturn;
			yield return null;
		}
	}
}