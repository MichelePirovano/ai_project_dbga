﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Decision
{
	public abstract class DecisionMaker : MonoBehaviour
	{
		public abstract void MakeDecision();
	}
	
	public class Brain : MonoBehaviour
	{
		public float tickDelay = 1f;
		public int currentOrder = 0; // Order given by the boss
		
		public DecisionMaker decisionMaker;
		
		void Start()
		{
			InvokeRepeating("TickBrain", tickDelay, tickDelay);
		}

		void TickBrain()
		{
			decisionMaker.MakeDecision();
		}
	}
}

