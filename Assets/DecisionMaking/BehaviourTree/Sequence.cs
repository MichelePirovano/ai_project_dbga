﻿namespace AI.Decision.BT
{
	public class Sequence : Composite
	{
		public override TaskState Run()
		{
			foreach (var child in children)
			{
				if (child.DoRun() == TaskState.Fail) return TaskState.Fail;
			}
			return TaskState.Success;
		}
	}
}