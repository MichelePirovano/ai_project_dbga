﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Decision.BT
{
    public class BehaviourTree_DecisionMaker : DecisionMaker
    {
        public Task rootTask;

        void Awake()
        {
            BuildTree(rootTask);
        }
        
        void BuildTree(Task parentTask)
        {
            parentTask.myBrain = GetComponent<Brain>();
            //Debug.LogError("Found task "  + parentTask.name);
            if (parentTask.transform.childCount > 0)
            {
                Composite composite = parentTask as Composite;
                composite.children = new List<Task>();
                foreach (Transform childTr in composite.transform)
                {
                    Task childTask = childTr.GetComponent<Task>();
                    composite.children.Add(childTask);
                    BuildTree(childTask);
                }
            }
        }
        
        public override void MakeDecision()
        {
            //var result = rootTask.DoRun();
            //Debug.LogError($"Result of BT: {result}");
        }

        void Update()
        {
            var result = rootTask.DoRun();
            Debug.LogError($"Result of BT: {result}");
        }
    }
}
