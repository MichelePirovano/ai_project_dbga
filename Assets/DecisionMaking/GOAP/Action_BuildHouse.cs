﻿namespace AI.Decision.GOAP
{
	public class Action_BuildHouse : Action
	{
		protected override void Awake()
		{
			base.Awake();
			preconditions["wood"] = 10;		 // >=
			effects["wood"] = -10;
			effects["house"] = +1;
		}

		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
			agent.currentHouses += effects["house"];
		}
	}
}