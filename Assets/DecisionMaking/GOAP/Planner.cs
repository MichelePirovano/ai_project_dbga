﻿using System.Collections.Generic;
using System.Linq;

namespace AI.Decision.GOAP
{
	public class Planner
	{
		private class Node
		{
			public Node parent;
			public Dictionary<string, int> state;
			public float totalCost;	// Total cost to reach that node
			public Action action; // Action to reach this node

			public Node(Node _parent, Dictionary<string, int> _state, float _totalCost, Action _action)
			{
				this.parent = _parent;
				this.state = _state;
				this.totalCost = _totalCost;
				this.action = _action;
			}
		}
		
		
		public List<Action> GetPlan(List<Action> availableActions,
			Dictionary<string, int> startState,
			Dictionary<string, int> goalState)
		{
			// Build the tree of all actions until we find the goal
			List<Node> leaves = new List<Node>();
			Node startNode = new Node(null, startState, 0, null);
			bool canFindPlan = BuildSubTree(startNode, leaves, availableActions, goalState);
			if (!canFindPlan)
			{
				// No plan can be found? Return no plan!
				return new List<Action>();
			}
			
			// Find the best leaf among nodes that bring us to the goal state
			Node cheapest = null;
			foreach (var leaf in leaves)
			{
				if (cheapest == null)
					cheapest = leaf;
				else
				{
					if (leaf.totalCost < cheapest.totalCost)
						cheapest = leaf;
				}
			}
			
			// Find the plan from the cheapest leaf back to the root
			List<Action> plan = new List<Action>();
			Node n = cheapest;
			while (n != null)
			{
				if (n.action != null)
				{
					// Insert the action in the front
					plan.Insert(0, n.action);
				}
				n = n.parent;
			}
			return plan;
		}

		private bool BuildSubTree(Node parent, List<Node> leaves, List<Action> usableActions, Dictionary<string, int> goalState)
		{
			if (leaves.Any(l => l.totalCost < parent.totalCost))
			{
				return false;
			}
			
			bool foundSolution = false;
			foreach (var usableAction in usableActions)
			{
				// Check if we cannot execute that action, given the state
				if (!MatchesPreconditions(usableAction.preconditions, parent.state)) continue;
				
				// We execute that action and find a new state
				Dictionary<string, int> newState = ApplyAction(parent.state, usableAction.effects);
				Node newNode = new Node(parent, newState, parent.totalCost + usableAction.actionCost, usableAction);

				if (MatchesState(goalState, newState))
				{
					// We found the goal state!
					leaves.Add(newNode);
					foundSolution = true;
				}
				else
				{
					if (BuildSubTree(newNode, leaves, usableActions, goalState))
					{
						foundSolution = true;
					}
				}
			}
			return foundSolution;
		}

		private bool MatchesState(Dictionary<string, int> state1, Dictionary<string, int> state2)
		{
			foreach (string key in state1.Keys)
			{
				if (state1[key] != state2[key])
				{
					return false;
				}
			}
			return true;
		}

		private bool MatchesPreconditions(Dictionary<string, int> preconditions, Dictionary<string, int> state)
		{
			foreach (KeyValuePair<string,int> precondition in preconditions)
			{
				bool match = false;
				int stateValue = state[precondition.Key];
				
				// Check if the state matches the precondition
				// NOTE: these conditions are only for examples purposes, the precondition itself should define the logic to use, not the value
				if (precondition.Value == 0)	
				{
					match = stateValue == 0;		// Must be == 0
				}
				else if (precondition.Value > 0)
				{
					match = stateValue >= precondition.Value;	// Must be >= the precondition
				}
				if (!match) return false;
			}
			return true;
		}
		
		private Dictionary<string, int> ApplyAction(Dictionary<string, int> state, Dictionary<string, int> effects)
		{
			// Copy the current state to avoid modifying the other state
			Dictionary<string,int> newState = new Dictionary<string, int>();
			foreach (var s in state)
				newState.Add(s.Key, s.Value);
			
			// Apply the effects
			foreach (var effect in effects)
			{
				newState[effect.Key] += effect.Value;
			}
			return newState;
		}
		
	}
}