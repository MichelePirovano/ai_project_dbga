﻿using UnityEngine;

namespace AI.Decision.BT
{
	public class Wait : Task
	{
		public float period = 3f;

		private float t = 0f;
		public override TaskState Run()
		{
			if (t >= period)
			{
				return TaskState.Success;
			}
			else
			{
				t += Time.deltaTime;
				return TaskState.Fail;
			}
		}
	}
}