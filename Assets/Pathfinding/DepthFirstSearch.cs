﻿using System.Collections;
using System.Collections.Generic;
using AI.Pathfinding;
using UnityEngine;

public class DepthFirstSearch : MonoBehaviour
{
    public GraphMaker graphMaker;

    public float step = 0.2f;
    
    void Start()
    {
        StartCoroutine(Search());
    }

    public IEnumerator Search()
    {
        List<Node> visitedNodes = new List<Node>();
        Queue<Node> queue = new Queue<Node>();
        queue.Enqueue(graphMaker.startNode);
        graphMaker.startNode.color = Color.yellow;

        bool pathFound = false;
        while (queue.Count > 0)
        {
            Node current = queue.Dequeue();
            current.color = Color.red;
            visitedNodes.Add(current);
            
            if (current == graphMaker.endNode)
            {
                pathFound = true;
                break;
            }

            var neighbours = graphMaker.GetNeighbours(current);
            foreach (var neighbour in neighbours)
            {
                if (visitedNodes.Contains(neighbour)) continue;
                if (queue.Contains(neighbour)) continue;
                
                queue.Enqueue(neighbour);
                neighbour.color = Color.yellow;
            }

            yield return new WaitForSeconds(step);
        }

        if (pathFound)
        {
            Debug.Log("Found a path!");
        }
        else
        {
            Debug.Log("No path found!");
        }
    }

}
