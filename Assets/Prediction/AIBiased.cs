﻿using System.Collections;
using UnityEngine;

namespace AI.NGrams
{
	public class AIBiased : Player
	{
		public Action BiasedAction;
		[Range(0, 1)] public float BiasProbability = 0.1f;
		
		public override IEnumerator GetAction(Answer answer)
		{
			if (Random.value <= BiasProbability)
			{
				answer.action = BiasedAction;
			}
			else
			{
				answer.action = (Action) Random.Range(1, 4);
			}
			yield break;
		}
	}
}