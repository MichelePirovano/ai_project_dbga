﻿using System.Collections;
using UnityEngine;

namespace AI.NGrams
{
	public class AIRandom : Player
	{
		public override IEnumerator GetAction(Answer answer)
		{
			answer.action = (Action) Random.Range(1, 4);
			yield break;
		}
	}
}