﻿using System.Collections;
using UnityEngine;

namespace AI.NGrams
{
	public class AIFixed : Player
	{
		public Action FixedAction;
		
		public override IEnumerator GetAction(Answer answer)
		{
			answer.action = FixedAction;
			yield return null;
		}
	}
}