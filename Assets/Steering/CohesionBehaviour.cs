﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Steering
{
	public class CohesionBehaviour : SteeringBehaviour
	{
		public bool verbose = false;
		
		public float radius = 1f;
		
		private Agent[] agents;

		void Start()
		{			
			agents = GameObject.FindObjectsOfType<Agent>();
		}
		
		
		public override SteeringOutput GetSteering()
		{
			SteeringOutput steering = new SteeringOutput();
			
			Vector2 totalCohesion = new Vector2();

			steering.inactive = true;
			
			// For each agent...
			foreach (var agent in agents)
			{
				if (agent == null || agent.gameObject == null) continue;
				if (agent.gameObject == gameObject) continue;
				
				// Check if it is inside the circle of given radius
				var myPos = transform.position;
				myPos.z = 0;
				var otherPos = agent.transform.position;
				otherPos.z = 0;
				
				if (Vector2.SqrMagnitude(otherPos - myPos) >= radius * radius) continue;

				// Flee from that agent  
				Vector2 cohesionVector = (otherPos - myPos);

				// Add that vector
				float ratio = cohesionVector.sqrMagnitude / (radius * radius); // [0,1]
				
				totalCohesion += cohesionVector.normalized * ratio;

				Debug.DrawLine(myPos, otherPos, Color.blue);
				
				steering.inactive = false;
			}

			steering.targetLinearVelocityPercent = Vector2.ClampMagnitude(totalCohesion, 1f);

			return steering;
		}
	}

}