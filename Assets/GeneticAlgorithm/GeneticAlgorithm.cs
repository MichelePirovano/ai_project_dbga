﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Genetics
{
	public class GeneticAlgorithm : MonoBehaviour
	{
		public int nGenerations = 10;
		public int simulationSeconds = 10;
		public int populationSize = 100;
		public int speedupTimescale = 10;
		
		public Individual prefabIndividual;
		
		public int minSizeX = 2;
		public int maxSizeX = 20;
		
		public int minSizeY = 2;
		public int maxSizeY = 20;

		public int maxRotationSpeed = 500;

		[Range(0, 1f)] 
		public float truncationPercentage = 0.3f;
		
		void Start()
		{
			// Create the initial population
			List<Genotype> initialGenotypes = new List<Genotype>();
			// .... create a random population
			for (int i = 0; i < populationSize; i++)
			{
				var g = new Genotype();
				g.bodySizeX = Random.Range(minSizeX, maxSizeX);
				g.bodySizeY = Random.Range(minSizeY, maxSizeY);
				g.legFrontSizeX = Random.Range(minSizeX, maxSizeX);
				g.legFrontSizeY = Random.Range(minSizeY, maxSizeY);
				g.legBackSizeX = Random.Range(minSizeX, maxSizeX);
				g.legBackSizeY = Random.Range(minSizeY, maxSizeY);
				g.legFrontMotorForce = Random.Range(-maxRotationSpeed, maxRotationSpeed);
				g.legBackMotorForce = Random.Range(-maxRotationSpeed, maxRotationSpeed);
				g.color = Random.Range(0, 101);
				initialGenotypes.Add(g);
			}

			StartCoroutine(GeneticCoroutine(initialGenotypes));
		}

		void Update()
		{
			if (Input.GetKeyDown(KeyCode.Alpha1)) Time.timeScale = 1f;
			if (Input.GetKeyDown(KeyCode.Alpha2)) Time.timeScale = speedupTimescale;
		}
		

		private IEnumerator GeneticCoroutine(List<Genotype> currentGenotypes)
		{
			for (int generation = 0; generation < nGenerations; generation++)
			{
				// Spawn the population...
				List<Individual> population = new List<Individual>();
				for (int iIndividual = 0; iIndividual < currentGenotypes.Count; iIndividual++)
				{
					var spawnedIndividual = Instantiate(prefabIndividual.gameObject).GetComponent<Individual>();
					spawnedIndividual.SetGenotype(currentGenotypes[iIndividual]);
					spawnedIndividual.transform.position = new Vector3(0,0, iIndividual);
					population.Add(spawnedIndividual);
				}
				
				// Simulate the world...
				yield return new WaitForSeconds(simulationSeconds);
				
				// Evaluate the fitness
				foreach (var individual in population)
				{
					individual.genotype.fitness = individual.fitness;
					Destroy(individual.gameObject);
				}
				
				// Selection: truncation at 30%
				currentGenotypes.Sort((x,y) => (int)(100*y.fitness - 100*x.fitness));
				List<Genotype> selectedGenotypes = currentGenotypes.GetRange(0, (int) (truncationPercentage * populationSize));

				// Crossover
				List<Genotype> crossoverGenotypes = new List<Genotype>();
				for (int ci = 0; ci < selectedGenotypes.Count/2; ci++)
				{
					Genotype p1 = selectedGenotypes[ci * 2];
					Genotype p2 = selectedGenotypes[ci * 2 + 1];
					
					Genotype c1 = new Genotype();
					c1.bodySizeX = Crossover(p1.bodySizeX, p2.bodySizeX);
					c1.bodySizeY = Crossover(p1.bodySizeY, p2.bodySizeY);
					c1.legBackSizeX = Crossover(p1.legBackSizeX, p2.legBackSizeX);
					c1.legBackSizeY = Crossover(p1.legBackSizeY, p2.legBackSizeY);
					c1.legFrontSizeX = Crossover(p1.legFrontSizeX, p2.legFrontSizeX);
					c1.legFrontSizeY = Crossover(p1.legFrontSizeY, p2.legFrontSizeY);
					c1.legBackMotorForce = Crossover(p1.legBackMotorForce, p2.legBackMotorForce);
					c1.legFrontMotorForce = Crossover(p1.legFrontMotorForce, p2.legFrontMotorForce);
					c1.color = Crossover(p1.color, p2.color);

					Genotype c2 = new Genotype();
					c2.bodySizeX = Crossover(p1.bodySizeX, p2.bodySizeX);
					c2.bodySizeY = Crossover(p1.bodySizeY, p2.bodySizeY);
					c2.legBackSizeX = Crossover(p1.legBackSizeX, p2.legBackSizeX);
					c2.legBackSizeY = Crossover(p1.legBackSizeY, p2.legBackSizeY);
					c2.legFrontSizeX = Crossover(p1.legFrontSizeX, p2.legFrontSizeX);
					c2.legFrontSizeY = Crossover(p1.legFrontSizeY, p2.legFrontSizeY);
					c2.legBackMotorForce = Crossover(p1.legBackMotorForce, p2.legBackMotorForce);
					c2.legFrontMotorForce = Crossover(p1.legFrontMotorForce, p2.legFrontMotorForce);
					c2.color = Crossover(p1.color, p2.color);

					crossoverGenotypes.Add(c1);
					crossoverGenotypes.Add(c2);
				}
				
				// Mutation
				List<Genotype> mutatedGenotypes = new List<Genotype>();
				int nMutated = populationSize - selectedGenotypes.Count - crossoverGenotypes.Count;
				for (int mi = 0; mi < nMutated; mi++)
				{
					Genotype p = crossoverGenotypes[Random.Range(0, crossoverGenotypes.Count)];
				
					Genotype m = new Genotype();
					m.bodySizeX = p.bodySizeX;
					m.bodySizeY = p.bodySizeY;
					m.legBackSizeX = p.legBackSizeX;
					m.legBackSizeY = p.legBackSizeY;
					m.legFrontSizeX = p.legFrontSizeX;
					m.legFrontSizeY = p.legFrontSizeY;
					m.legBackMotorForce = p.legBackMotorForce;
					m.legFrontMotorForce = p.legFrontMotorForce;
					m.color = p.color;

					for (int i = 0; i < 5; i++)
					{
						int choice = Random.Range(0, 9);
						switch (choice)
						{
							case 0: m.bodySizeX = Random.Range(minSizeX, maxSizeX); break;
							case 1: m.bodySizeY = Random.Range(minSizeY, maxSizeY); break;
							case 2: m.legBackSizeX = Random.Range(minSizeX, maxSizeX); break;
							case 3: m.legBackSizeY = Random.Range(minSizeY, maxSizeY); break;
							case 4: m.legFrontSizeX = Random.Range(minSizeX, maxSizeX); break;
							case 5: m.legFrontSizeY = Random.Range(minSizeY, maxSizeY); break;
							case 6: m.legBackMotorForce = Random.Range(-maxRotationSpeed, maxRotationSpeed); break;
							case 7: m.legFrontMotorForce = Random.Range(-maxRotationSpeed, maxRotationSpeed); break;
							case 8: m.color = Random.Range(0, 101); break;
						}
					}
					
					mutatedGenotypes.Add(m);
				}
				
				currentGenotypes = new List<Genotype>();
				currentGenotypes.AddRange(selectedGenotypes);
				currentGenotypes.AddRange(crossoverGenotypes);
				currentGenotypes.AddRange(mutatedGenotypes);
				
				Debug.LogError($"Best individual at generation {generation} has fitness: {selectedGenotypes[0].fitness}");
			}
			
		}

		int Crossover(int v1, int v2)
		{
			return Random.value > 0.5f ? v1 : v2;
			//c1.bodySizeX = (int)Mathf.Lerp(p1.bodySizeX, p2.bodySizeX, Random.value);
		}
	}
}