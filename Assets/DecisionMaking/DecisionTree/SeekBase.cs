﻿using System.Collections;
using AI.Steering;
using UnityEngine;


namespace AI.Decision.DecisionTree
{
	public class SeekBase : Action
	{
		public override void DoAction()
		{
			var seekBe = myBrain.GetComponent<SeekBehaviour>();
			var fleeBe = myBrain.GetComponent<FleeBehaviour>();
			var baseTransform = GameObject.FindObjectOfType<Base>().transform;	
			seekBe.weight = 1;
			fleeBe.weight = 0;
			seekBe.targetTransform = baseTransform;
		}
	}
}
