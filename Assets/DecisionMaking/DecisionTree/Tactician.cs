﻿using System.Collections;
using System.Collections.Generic;
using AI.Decision.DecisionTree;
using UnityEngine;

namespace AI.Decision
{
    
	public class Tactician : MonoBehaviour
	{
		public TacticianDecisionTree_DecisionMaker tree;
		public float tickDelay = 1f;
		
		void Awake()
		{
			InvokeRepeating("TickBrain", tickDelay, tickDelay);
		}

		void TickBrain()
		{
			var decisionMaker = new TacticianDecisionTree_DecisionMaker();
			decisionMaker.MakeDecision();
		}
	}

}
