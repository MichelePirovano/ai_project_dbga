﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace AI.Decision.BT
{
	public class HealthLowCondition : Task
	{
		public int healthTheshold = 4;
		
		public override TaskState Run()
		{
			var healthState = myBrain.GetComponent<HealthState>();
			if (healthState.health <= healthTheshold)
			{
				return TaskState.Success;
			}
			return TaskState.Fail;
		}
		
	}

    
}