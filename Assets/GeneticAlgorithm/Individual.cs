﻿using UnityEngine;
using Random = System.Random;

namespace AI.Genetics
{
	public class Genotype
	{
		public int bodySizeX = 1;
		public int bodySizeY = 1;
		public int legFrontSizeX = 1;
		public int legFrontSizeY = 1;
		public int legBackSizeX = 1;
		public int legBackSizeY = 1;
		
		public int legFrontMotorForce = 1;
		public int legBackMotorForce = 1;

		public int color = 1;	// [0,100]

		public float fitness;
	}
	
	
	public class Individual : MonoBehaviour
	{
		public HingeJoint2D jointFront;
		public HingeJoint2D jointBack;
		public Rigidbody2D body;
		public Rigidbody2D legFront;
		public Rigidbody2D legBack;

		public Genotype genotype;

		public float fitness;

		public float t;
		
		void Update()
		{
			if (genotype == null) return;
			
			if (body.transform.position.x > fitness)
			{
				fitness = body.transform.position.x;
			}

			t += Time.deltaTime;
			var motorBack = jointBack.motor;
			motorBack.motorSpeed = genotype.legBackMotorForce * Mathf.Sin(t * genotype.bodySizeX/10f);
			jointBack.motor = motorBack;
		}
		
		public void SetGenotype(Genotype genotype)
		{
			this.genotype = genotype;
			
			body.GetComponent<SpriteRenderer>().color = Color.HSVToRGB(genotype.color / 100f, 1, 1);
			legFront.GetComponent<SpriteRenderer>().color = Color.HSVToRGB(genotype.color / 100f, 1, 0.8f);
			legBack.GetComponent<SpriteRenderer>().color = Color.HSVToRGB(genotype.color / 100f, 1, 0.5f);
			/*
			body.transform.localScale = new Vector3(genotype.bodySizeX/10f, genotype.bodySizeY/10f, 1);
			body.transform.localPosition = new Vector3(0,  (genotype.bodySizeY/10f-1)/2f , 0); 
			legFront.transform.localScale = new Vector3(genotype.legFrontSizeX/10f, genotype.legFrontSizeY/10f, 1);
			legFront.transform.localPosition += new Vector3(0, (genotype.legFrontSizeY/10f)/2f , 0); 
			legBack.transform.localScale = new Vector3(genotype.legBackSizeX/10f, genotype.legBackSizeY/10f, 1);
			legBack.transform.localPosition += new Vector3(0, (genotype.legBackSizeY/10f)/2f , 0); 
			*/

			var motorFront = jointFront.motor;
			motorFront.motorSpeed = genotype.legFrontMotorForce;
			jointFront.motor = motorFront;
			
			var motorBack = jointBack.motor;
			motorBack.motorSpeed = genotype.legBackMotorForce;
			jointBack.motor = motorBack;
		}
		
	}

}
