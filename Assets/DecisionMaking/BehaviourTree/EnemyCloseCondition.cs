﻿using System.Collections;
using System.Collections.Generic;
using AI.Steering;
using UnityEngine;


namespace AI.Decision.BT
{
	public class EnemyCloseCondition : Task
	{
		public float minDistance = 15f;
		
		public override TaskState Run()
		{
			var allAgents = GameObject.FindObjectsOfType<Agent>();
			foreach (var otherAgent in allAgents)
			{
				if (otherAgent == myBrain.GetComponent<Agent>()) continue;
				if (otherAgent.GetComponent<HealthState>().team == myBrain.GetComponent<HealthState>().team) continue;

				if (Vector2.SqrMagnitude(otherAgent.transform.position - myBrain.transform.position) < minDistance * minDistance)
				{
					return TaskState.Success;
				}
			}

			return TaskState.Fail;
		}
	}
    
}