﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace AI.PCG.Noise
{
	public class TexturedQuad : MonoBehaviour
	{
		public int width;
		public int height;

		public int perlinWidth = 10;
		public int perlinHeight = 10;
		public int nOctaves = 1;
		public float threshold = 0.3f;
		
		IEnumerator Start()
		{
			var texture = new Texture2D(width, height, TextureFormat.RGB24, false);
			texture.filterMode = FilterMode.Point;
			GetComponent<MeshRenderer>().material.mainTexture = texture;

			while (true)
			{
				for (int x = 0; x < width; x++)
				{
					for (int y = 0; y < height; y++)
					{
						//float v = UnityEngine.Random.value;
						float v = 0f;
						for (int oct = 1; oct <= nOctaves; oct++)
						{
							if (oct == 1)
							{
								v += Mathf.PerlinNoise(Time.time+ oct * perlinWidth * x / (float)width, oct * perlinHeight * y /  (float)height);
							}
							else
							{
								var noise = Mathf.PerlinNoise(Time.time+oct * perlinWidth * x / (float)width, oct * perlinHeight * y /  (float)height);
								noise = ((2 * noise) - 1);	// [0,1] to [-1,1]
								noise /= oct;	// Diminish amplitude
								v += noise;
							}
						}

						var color = Color.white;
					
						if (v < threshold)
						{
							color = Color.Lerp(new Color(0, 0.2f, 0.5f, 1f), Color.cyan, v/threshold);
						}
						else
						{
							color = Color.Lerp(new Color(0.7f, 0.7f, 0.2f, 1f), new Color(0f,0.8f,0f,1f), (v-threshold)/(1-threshold));
						}
					
						texture.SetPixel(x,y,color);
					}
				}
				texture.Apply();
				yield return null;
			}
		}

	}
}
