﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavmeshTarget : MonoBehaviour
{
    public NavMeshAgent agent;
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var rayFromCameraToMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayHit = new RaycastHit();
            if (Physics.Raycast(rayFromCameraToMouse, out rayHit, 1000))
            {
                transform.position = rayHit.point;
                var agents = GameObject.FindObjectsOfType<NavMeshAgent>();
                foreach (var navMeshAgent in agents)
                {
                    navMeshAgent.SetDestination(transform.position);
                }
            }
        }
    }
}
