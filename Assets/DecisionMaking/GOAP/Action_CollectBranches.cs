﻿namespace AI.Decision.GOAP
{
	public class Action_CollectBranches : Action
	{
		protected override void Awake()
		{
			base.Awake();
			effects["wood"] = +1;
		}

		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
		}
	}
}