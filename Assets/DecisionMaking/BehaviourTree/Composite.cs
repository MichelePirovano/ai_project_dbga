﻿using System.Collections.Generic;

namespace AI.Decision.BT
{
	public abstract class Composite : Task
	{
		public List<Task> children = new List<Task>();
	}
}