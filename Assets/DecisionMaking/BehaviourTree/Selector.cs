﻿using System.Collections.Generic;

namespace AI.Decision.BT
{
	public class Selector : Composite
	{
		public override TaskState Run()
		{
			foreach (var child in children)
			{
				if (child.DoRun() == TaskState.Success) return TaskState.Success;
			}
			return TaskState.Fail;
		}
	}
}