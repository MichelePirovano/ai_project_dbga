﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.NGrams
{
	public class AINgram : Player
	{
		public int NGamesBeforePrediction = 10;

		private Dictionary<string, int[]> nGrameTable = new Dictionary<string, int[]>();
		private List<Action> actionsWindow = new List<Action>();
		
		void Awake()
		{
			for (int action1 = 1; action1 <= 3; action1++)
			{
				for (int action2 = 1; action2 <= 3; action2++)
				{
					var key = $"{action1}{action2}";
					var value = new int[3];
					nGrameTable.Add(key, value);
				}
			}
			
		}

		private int nGames = 0;
		
		public override IEnumerator GetAction(Answer answer)
		{
			nGames++;
			if (nGames < NGamesBeforePrediction)
			{
				answer.action = (Action) Random.Range(1, 4);
			}
			else
			{
				// Let's look at the last two actions
				Action a1 = actionsWindow[0];
				Action a2 = actionsWindow[1];
				var key = $"{(int)a1}{(int)a2}";
				
				// Find the counts of actions we observed
				int[] opponentActionCounts = nGrameTable[key];

				// Define the probability of choosing an action based on what we observed
				int totalOpponentActions = opponentActionCounts.Sum();
				float[] ourActionProbabilities = new float[3];
				ourActionProbabilities[(int) (Action.ROCK - 1)] = opponentActionCounts[(int) (Action.SCISSORS) - 1] / (float) totalOpponentActions;
				ourActionProbabilities[(int) (Action.PAPER - 1)] = opponentActionCounts[(int) (Action.ROCK) - 1] / (float) totalOpponentActions;
				ourActionProbabilities[(int) (Action.SCISSORS - 1)] = opponentActionCounts[(int) (Action.PAPER) - 1] / (float) totalOpponentActions;

				// Choose randomly using those probabilities
				float rndChoice = UnityEngine.Random.value; // [0,1]
				if (rndChoice <= ourActionProbabilities[(int) (Action.ROCK - 1)])
				{
					answer.action = Action.ROCK;
				}
				else if (rndChoice <=  ourActionProbabilities[(int) (Action.ROCK - 1)] +  ourActionProbabilities[(int) (Action.PAPER -1)])
				{
					answer.action = Action.PAPER;
				}
				else
				{
					answer.action = Action.SCISSORS;
				}
			}
			yield break;
		}

		public override void ReceiveOpponentAction(Action a3)
		{
			if (actionsWindow.Count >= 2)
			{
				// Fill the table
				var a1 = actionsWindow[0];
				var a2 = actionsWindow[1];
				var key = $"{(int)a1}{(int)a2}";
				//Debug.LogError(key);
				nGrameTable[key][(int) a3 - 1] += 1;
			}
			
			// Add to the queue
			actionsWindow.Add(a3);	// CS
			if (actionsWindow.Count == 3)
			{
				actionsWindow.RemoveAt(0);
			}
		}
	}
}