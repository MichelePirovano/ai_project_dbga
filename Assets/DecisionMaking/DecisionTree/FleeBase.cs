﻿using AI.Steering;
using UnityEngine;

namespace AI.Decision.DecisionTree
{
	public class FleeBase : Action
	{
		public override void DoAction()
		{
			var fleeBe = myBrain.GetComponent<FleeBehaviour>();
			var seekBe = myBrain.GetComponent<SeekBehaviour>();
			var baseTransform = GameObject.FindObjectOfType<Base>().transform;	
			fleeBe.weight = 1;
			seekBe.weight = 0;
			fleeBe.targetTransform = baseTransform;	// We'll flee the base
		}
	}
}