﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.PCG.Noise
{
	public class NoiseVisualizer : MonoBehaviour
	{
		private Vector3 startPos;
		
		void Awake()
		{
			startPos = transform.position;
		}

		public float frequency = 1f;
		public float phase = 0f;
		
		void Update()
		{
			transform.position = startPos +
			                     new Vector3(
				                     Time.time,
				                     Mathf.PerlinNoise(frequency * Time.time + phase, 0f),
				                     0);
		}
	}

}
