﻿using System.Collections;
using UnityEngine;

namespace AI.Decision.BT
{
	public abstract class Task : MonoBehaviour
	{
		public Brain myBrain;

		public TaskState DoRun()
		{
			Debug.Log(this.name);
			return Run();
		}
		
		public abstract TaskState Run();
		
		
	}
}
