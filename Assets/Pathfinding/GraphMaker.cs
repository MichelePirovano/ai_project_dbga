﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.Pathfinding
{
    public class GraphMaker : MonoBehaviour
    {
        public float radius = 1f;

        public List<Node> nodes;
        public List<Edge> edges;

        public Node startNode;
        public Node endNode;


        public List<Node> GetNeighbours(Node fromNode)
        {
            List<Node> neighbours = new List<Node>();
            foreach (var edge in edges)
            {
                if (edge.nodeA == fromNode)
                {
                    neighbours.Add(edge.nodeB);
                }
                if (edge.nodeB == fromNode)
                {
                    neighbours.Add(edge.nodeA);
                }
            }
            return neighbours;
        }
        
        void Awake()
        {
            nodes = FindObjectsOfType<Node>().ToList();
            edges = new List<Edge>();
            
            foreach (var nodeA in nodes)
            {
                foreach (var nodeB in nodes)
                {
                    if (nodeA == nodeB) continue;

                    if (Vector2.SqrMagnitude(nodeA.transform.position - nodeB.transform.position) < radius*radius)
                    {
                        //Debug.DrawLine(nodeA.transform.position, nodeB.transform.position, Color.white, 10f);

                        var edgeGO = new GameObject("Edge");
                        var edge = edgeGO.AddComponent<Edge>();
                        edge.nodeA = nodeA;
                        edge.nodeB = nodeB;

                        if (!edges.Contains(edge))
                        {
                            edges.Add(edge);
                        }
                    }
                }
            }
            
            foreach (var edge in edges)
            {
                edge.color = Color.gray;
            }

        }

        void Update()
        {
            foreach (var node in nodes)
            {
                node.Draw();
            }
            
            foreach (var edge in edges)
            {
                edge.Draw();
            }
        }

    }

}
