﻿using System.Collections;
using System.Collections.Generic;
using AI.Steering;
using UnityEngine;

namespace AI.Decision.BT
{
	public class Flee : Task
	{
		public override TaskState Run()
		{
			var fleeBe = myBrain.GetComponent<FleeBehaviour>();
			var seekBe = myBrain.GetComponent<SeekBehaviour>();
			var baseTransform = GameObject.FindObjectOfType<Base>().transform;	
			fleeBe.weight = 1;
			seekBe.weight = 0;
			fleeBe.targetTransform = baseTransform;	// We'll flee the base
			return TaskState.Success;
		}
	}

}