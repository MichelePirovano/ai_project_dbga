﻿using AI.Steering;
using UnityEngine;

namespace AI.Decision.BT
{
	public class ShootEnemy : Task
	{
		public override TaskState Run()
		{
			var allAgents = GameObject.FindObjectsOfType<Agent>();

			Agent closestAgent = null;
			var minDistance = Mathf.Infinity;
			
			foreach (var otherAgent in allAgents)
			{
				if (otherAgent == myBrain.GetComponent<Agent>()) continue;
				if (otherAgent.GetComponent<HealthState>().team == myBrain.GetComponent<HealthState>().team) continue;

				var dist = Vector2.Distance(otherAgent.transform.position, myBrain.transform.position);
				if (dist < minDistance)
				{
					minDistance = dist;
					closestAgent = otherAgent;
				}
			}

			if (closestAgent != null)
			{
				myBrain.GetComponent<Shooter>().StartShooting();
				var seekBe = myBrain.GetComponent<SeekBehaviour>();
				seekBe.weight = 1;
				seekBe.targetTransform = closestAgent.transform;
				myBrain.GetComponent<Agent>().maximumLinearVelocity = 0;	// Seek, without moving
			}
			
			return TaskState.Success;
		}
	}
}