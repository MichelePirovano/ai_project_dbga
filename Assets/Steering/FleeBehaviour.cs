﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Steering
{
	public class FleeBehaviour : SteeringBehaviour
	{
		public Transform targetTransform;

		public override SteeringOutput GetSteering()
		{
			SteeringOutput steering = new SteeringOutput();
			if (targetTransform == null) return steering;
			var direction = (targetTransform.position - transform.position);
			direction.z = 0;
			steering.targetLinearVelocityPercent = -direction.normalized;
			return steering;
		}
	}

}