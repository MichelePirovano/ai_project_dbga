﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Steering
{
	public struct SteeringOutput
	{
		public bool inactive;
		public Vector2 targetLinearVelocityPercent;
	}
	
	
	public abstract class SteeringBehaviour : MonoBehaviour
	{
		[Range(0,2)]
		public float weight;
		
		public abstract SteeringOutput GetSteering();
	}

}