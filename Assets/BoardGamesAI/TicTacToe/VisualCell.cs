﻿using UnityEngine;
using UnityEngine.UI;

namespace AI.TicTacToe
{
	public class VisualCell : MonoBehaviour
	{
		public Image image;
		public Sprite xSprite;
		public Sprite ySprite;

		void Awake()
		{
			image = GetComponent<Image>();
		}
		
		public void SetValue(CellValue v)
		{
			image.enabled = v != CellValue.E;
			image.sprite = v == CellValue.X ? xSprite : ySprite;
		}
	}
}