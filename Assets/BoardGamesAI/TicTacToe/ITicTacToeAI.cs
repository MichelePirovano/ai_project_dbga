﻿using System.Collections;
using System.Collections.Generic;

namespace AI.TicTacToe
{
	public interface ITicTacToeAI
	{
		IEnumerator GetBestAction(TicTacToe game, State state, Action outputAction);
	}
}