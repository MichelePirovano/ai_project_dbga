﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AI.Decision.GOAP
{
	public class Agent : MonoBehaviour
	{
		private List<Action> availableActions;
		private Planner planner;

		// Goal
		public int targetWood = 30;
		public int targetHouses = 1;
		
		// State
		public int currentWood = 0;
		public int axeDurability = 0;
		public int currentHouses = 0;

		public void Start()
		{
			availableActions = GetComponentsInChildren<Action>().ToList();
			planner = new Planner();
			StartCoroutine(LifeCO());
		}

		private IEnumerator LifeCO()
		{
			// define the plan for my life...
			Dictionary<string,int> startState = new Dictionary<string, int>();
			startState["wood"] = currentWood;
			startState["durability"] = axeDurability;
			startState["house"] = currentHouses;
			
			Dictionary<string,int> goalState = new Dictionary<string, int>();
			goalState["wood"] = targetWood;
			goalState["house"] = targetHouses;

			List<Action> plan = planner.GetPlan(availableActions, startState, goalState);
			Debug.Log("We have a plan with " + plan.Count + " actions");

			while (true)
			{
				if (plan.Count > 0)
				{
					var chosenAction = plan[0];
					plan.RemoveAt(0);
					Debug.LogWarning($"Performing action {chosenAction.GetType().Name}");
					chosenAction.Perform(this);
				}
				else
				{
					Debug.LogWarning("No action planned!");
				}
				yield return new WaitForSeconds(0.1f);
			}
		}
	}
}