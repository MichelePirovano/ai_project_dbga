﻿using System.Collections;
using System.Collections.Generic;
using AI.Steering;
using UnityEngine;

namespace AI.Decision.DecisionTree
{
	public class HealthLowCondition : Decision
	{
		public int healthTheshold = 4;
		
		public override bool CheckCondition()
		{
			var healthState = myBrain.GetComponent<HealthState>();
			if (healthState.health <= healthTheshold)
			{
				return true;
			}
			return false;
		}
	}

}
