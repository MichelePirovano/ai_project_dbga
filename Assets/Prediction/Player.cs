﻿using System.Collections;
using UnityEngine;

namespace AI.NGrams
{
	public abstract class Player : MonoBehaviour
	{
		public abstract IEnumerator GetAction(Answer answer);
		public virtual void ReceiveOpponentAction(Action a3) { }
	}
}