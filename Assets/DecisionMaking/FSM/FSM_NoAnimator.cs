﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace AI.Decision.FSM
{
	public abstract class State
	{
		public abstract void Handle();

		public virtual void OnEnter(){}
		public virtual void OnExit(){}
	}

	public class State_Example1 : State
	{
		public override void Handle()
		{
			Debug.LogError("Executing State_Example 1!");
		}
	}
	public class State_Example2 : State
	{
		public override void Handle()
		{
			Debug.LogError("Executing State_Example 2!");
		}
	}

	public class Transition
	{
		public string command; // could be an enumator
		public State currentState;
		public State nextState;
	}
    
	public class FSM_NoAnimator : DecisionMaker
	{
		public List<State> states = new List<State>();
		public List<Transition> transitions = new List<Transition>();
		public State currentState;
		bool initialised;

		private void Initialise()
		{
			if (initialised) return;
			initialised = true;
			
			// Add all states
			states.Add(new State_Example1());
			states.Add(new State_Example2());

			// Add all transitions
			transitions.Add(new Transition {command = "TestInput", currentState = states[0], nextState = states[1]});

			currentState = states[0];
		}
		
		public override void MakeDecision()
		{
			Initialise();
			
			// Gather inputs and traverse transitions
			var testInput = Input.GetKey(KeyCode.A);
			var chosenCommand = testInput ? "TestInput" : "";
			foreach (var transition in transitions)
			{
				if (transition.command == chosenCommand && currentState == transition.currentState)
				{
					currentState.OnExit();
					currentState = transition.nextState;
					currentState.OnEnter();
				}
			}
		}
		
		void Update()
		{
			Initialise();
			
			// Execute the current state
			currentState.Handle();
		}
	}

}
