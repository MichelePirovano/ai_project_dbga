﻿using System.Collections.Generic;

namespace AI.Decision.BT
{
	public class NotDecorator : Composite
	{
		public override TaskState Run()
		{
			var result = children[0].DoRun();
			if (result == TaskState.Fail) return TaskState.Success;
			else if (result == TaskState.Success) return TaskState.Fail;
			else return result;
		}
	}
}