﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

namespace AI.TicTacToe
{
    public enum CellValue
    {
        E = 0,
        X = 1,
        O = -1,
        
        Draw = 2,
    }

    [Serializable]
    public class State
    {
        public CellValue player;
        public CellValue[,] cells = new CellValue[3,3];
    }

    public class Action
    {
        public int i;
        public int j;
        public CellValue value;

        public override string ToString()
        {
            return $"({i},{j} : {value})";
        }
    }

    public class TicTacToe : MonoBehaviour
    {
        public CellValue[] initialStateArray;
        public CellValue startingPlayer;
        
        public VisualBoard board;
        
        IEnumerator Start()
        {
            State initialState = new State();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    int index = i * 3 + j;
                    initialState.cells[i, j] = initialStateArray[index];
                }
            }
            initialState.player = startingPlayer;
            board.ShowState(initialState);

            //////////
            // Our AI is here!
            ITicTacToeAI AI = gameObject.AddComponent<MiniMaxing>();
            Action chosenAction = new Action();
            yield return AI.GetBestAction(this, initialState, chosenAction);
            //////////
            
            //var nextState = StateTransition(initialState, chosenAction);
            //board.ShowState(nextState);
        }
        
        
        public List<Action> GetPossibleActions(State s)
        {
            List<Action> possibleActions = new List<Action>();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (s.cells[i, j] == CellValue.E)
                    {
                        Action action = new Action();
                        action.i = i;
                        action.j = j;
                        action.value = s.player;
                        possibleActions.Add(action);
                    }
                }
            }
            return possibleActions;
        }

        public State StateTransition(State s1, Action action)
        {
            State s2 = new State();
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    s2.cells[i, j] = s1.cells[i,j];
                }
            }
            s2.cells[action.i, action.j] = action.value;
            s2.player = (CellValue) (-(int) s1.player);
            return s2;
        }

        public bool IsTerminalState(State s, out CellValue winner)
        {
            // Horizontal
            for (int i = 0; i < 3; i++)
            {
                if (s.cells[i, 0] != CellValue.E
                    && s.cells[i, 0] == s.cells[i, 1]
                    && s.cells[i, 1] == s.cells[i, 2])
                {
                    winner = s.cells[i, 0];
                    return true;
                }
            }
            
            // Vertical
            for (int j = 0; j < 3; j++)
            {
                if (s.cells[0, j] != CellValue.E
                    && s.cells[0, j] == s.cells[1, j]
                    && s.cells[1, j] == s.cells[2, j])
                {
                    winner = s.cells[0, j];
                    return true;
                }
            }
            
            // Diagonals
            if (s.cells[0, 0] != CellValue.E
                && s.cells[0, 0] == s.cells[1, 1]
                && s.cells[1, 1] == s.cells[2, 2])
            {
                winner = s.cells[0, 0];
                return true;
            }
            if (s.cells[0, 2] != CellValue.E
                && s.cells[0, 2] == s.cells[1, 1]
                && s.cells[1, 1] == s.cells[2, 0])
            {
                winner = s.cells[0, 2];
                return true;
            }
            
            // Check for draws
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (s.cells[i, j] == CellValue.E)
                    {
                        winner = CellValue.E;
                        return false;
                    }
                }
            }
            
            winner = CellValue.Draw;
            return true;
        }
    }


}
