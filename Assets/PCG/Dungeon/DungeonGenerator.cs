﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.PCG.Dungeon
{
	public enum Direction
	{
		N = 0, 
		E = 1, 
		S = 2, 
		W = 3
	}
	
	public class DungeonGenerator : MonoBehaviour
	{
		public int Width = 10;
		public int Height = 10;
		
		public DungeonCell prefabCell;
		public DungeonCell[,] cells;

		public float waitPeriod = 0.1f;
		
		void Start()
		{
			cells = new DungeonCell[Width,Height];
			for (int i = 0; i < Width; i++)
			{
				for (int j = 0; j < Height; j++)
				{
					GameObject go = Instantiate(prefabCell.gameObject);
					go.transform.position = new Vector3(i, j);
					cells[i, j] = go.GetComponent<DungeonCell>();
					cells[i, j].i = i;
					cells[i, j].j = j;
					cells[i,j].SetWall();
				}
			}

			StartCoroutine(AlgorithmCO());
		}

		public int startI;
		public int startJ;

		private Direction lastDirection;
		
		private IEnumerator AlgorithmCO()
		{
			// All cells start as walls
			// Choose a start cell
			cells[startI,startJ].SetFloor();
			
			Stack<DungeonCell> stack = new Stack<DungeonCell>();
			stack.Push(cells[startI,startJ]);

			while (stack.Count > 0)
			{
				// Peek at the top cell of the stack
				DungeonCell currentCell = stack.Peek();
				
				currentCell.SetTarget();
				yield return new WaitForSeconds(waitPeriod);
				currentCell.SetFloor();
				
				// Check the possible directions
				List<Direction> possibleDirections = GetPossibleDirections(currentCell.i, currentCell.j);

				
				if (possibleDirections.Count == 0)
				{
					// No more directions: backtracking
					stack.Pop();
				}
				else
				{
					// Choose a direction at random / last one if possible
					Direction dir;
					if (possibleDirections.Contains(lastDirection) && UnityEngine.Random.value > 0.3f)
					{
						dir = lastDirection;
					}
					else
					{
						int rndIndex = UnityEngine.Random.Range(0, possibleDirections.Count);
						dir = possibleDirections[rndIndex];
					}
					lastDirection = dir;
					
					// We dig to the new cell
					var newCell = Dig(currentCell, dir);

					// We add the new cell to the stack
					stack.Push(newCell);
				}


			}
			yield break;
		}

		private DungeonCell Dig(DungeonCell fromCell, Direction dir)
		{
			DungeonCell toCell = null;
			DungeonCell midCell = null;

			switch (dir)
			{
				case Direction.N:
					toCell = cells[fromCell.i, fromCell.j + 2];
					midCell = cells[fromCell.i, fromCell.j + 1];
					break;
				case Direction.E:
					toCell = cells[fromCell.i + 2, fromCell.j];
					midCell = cells[fromCell.i + 1, fromCell.j];
					break;
				case Direction.S:
					toCell = cells[fromCell.i, fromCell.j - 2];
					midCell = cells[fromCell.i, fromCell.j - 1];
					break;
				case Direction.W:
					toCell = cells[fromCell.i - 2, fromCell.j];
					midCell = cells[fromCell.i - 1, fromCell.j];
					break;
			}
			toCell.SetFloor();
			midCell.SetFloor();
			return toCell;
		}

		private List<Direction> GetPossibleDirections(int i, int j)
		{
			List<Direction> list = new List<Direction>();

			// N
			if (j + 2 < Height && cells[i, j + 2].isWall)
			{
				list.Add(Direction.N);
			}
			
			// E
			if (i + 2 < Width && cells[i + 2, j].isWall)
			{
				list.Add(Direction.E);
			}

			// S
			if (j - 2 >= 0 && cells[i, j - 2].isWall)
			{
				list.Add(Direction.S);
			}

			// W
			if (i - 2 >= 0 && cells[i - 2, j].isWall)
			{
				list.Add(Direction.W);
			}

			return list;
		}
	}

}
