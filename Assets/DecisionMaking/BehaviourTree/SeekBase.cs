﻿using AI.Steering;
using UnityEngine;

namespace AI.Decision.BT
{
	public class SeekBase : Task
	{
		public override TaskState Run()
		{
			var seekBe = myBrain.GetComponent<SeekBehaviour>();
			var fleeBe = myBrain.GetComponent<FleeBehaviour>();
			var baseTransform = GameObject.FindObjectOfType<Base>().transform;	
			seekBe.weight = 1;
			fleeBe.weight = 0;
			seekBe.targetTransform = baseTransform;
			return TaskState.Success;
		}
	}
}