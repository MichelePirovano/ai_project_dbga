﻿using System.Collections;
using UnityEngine;

namespace AI.NGrams
{
	public class HumanPlayer : Player
	{
		public override IEnumerator GetAction(Answer answer)
		{
			while (true)
			{
				if (Input.GetKeyDown(KeyCode.S))
				{
					answer.action = Action.ROCK;
					break;
				}
				
				if (Input.GetKeyDown(KeyCode.C))
				{
					answer.action = Action.PAPER;
					break;
				}

				if (Input.GetKeyDown(KeyCode.F))
				{
					answer.action = Action.SCISSORS;
					break;
				}
				yield return null;
			}
		}
	}
}