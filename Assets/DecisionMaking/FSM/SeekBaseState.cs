﻿using System.Collections;
using System.Collections.Generic;
using AI.Decision;
using AI.Steering;
using UnityEngine;

public class SeekBaseState : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var myBrain = animator.GetComponentInParent<Brain>();
        var seekBe = myBrain.GetComponent<SeekBehaviour>();
        var fleeBe = myBrain.GetComponent<FleeBehaviour>();
        var baseTransform = GameObject.FindObjectOfType<Base>().transform;	
        seekBe.weight = 1;
        fleeBe.weight = 0;
        seekBe.targetTransform = baseTransform;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

}
