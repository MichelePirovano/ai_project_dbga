﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Steering
{
      
    public class Agent : MonoBehaviour
    {
        // Parameters
        public float maximumLinearVelocity = 1f;
        public float maximumAngularVelocity = 90f;

        public float maximumLinearAcceleration = 1f;
        public float maximumLinearDeceleration = 1f;
        public float maximumAngularAcceleration = 90f;

        public float velocityCheckThreshold = 0f;
        public float angleThresold = 5f;

        public float stopThreshold = 1f;
        
        // State
        public Vector2 linearVelocity;
        public float angularVelocity;

        public float currentLinearVelocity;
        
        
        public float linearAcceleration;
        public float angularAcceleration;
        
        
        private SteeringBehaviour[] steeringBehaviours;
        
        void Start()
        {
            steeringBehaviours = GetComponentsInChildren<SteeringBehaviour>();
        }

        void Update()
        {
            // Get the steering output
            Vector2 totalSteering = Vector2.zero;
            float totalWeight = 0;
            foreach (var steeringBehaviour in steeringBehaviours)
            {
                var output = steeringBehaviour.GetSteering();
                if (output.inactive) continue;
                
                totalSteering += steeringBehaviour.GetSteering().targetLinearVelocityPercent * steeringBehaviour.weight;
                totalWeight += steeringBehaviour.weight;
            }
            
            var targetVelocityPercent = totalSteering / totalWeight;
            if (totalWeight == 0) targetVelocityPercent = Vector2.zero;
            
            var targetVelocity = targetVelocityPercent * maximumLinearVelocity;

            Debug.DrawLine(transform.position, transform.position + (Vector3) targetVelocity * 10, Color.white);
            
            if (targetVelocity.sqrMagnitude > currentLinearVelocity * currentLinearVelocity + velocityCheckThreshold)
            {
                linearAcceleration = maximumLinearAcceleration;
            }
            else if (targetVelocity.sqrMagnitude < currentLinearVelocity * currentLinearVelocity - velocityCheckThreshold)
            {
                linearAcceleration = -maximumLinearDeceleration;
            }
            else
            {
                linearAcceleration = 0;
            }

            Vector3 crossVector = Vector3.Cross(targetVelocityPercent, transform.right);
            float angle = Mathf.Abs(Vector3.Angle(targetVelocityPercent, transform.right));
            int rotationDirection = (int) Mathf.Sign(crossVector.z);
            
            float accelerationRatio = Mathf.Clamp(angle, 2f, angleThresold) / angleThresold;
            angularAcceleration = rotationDirection * maximumAngularAcceleration * accelerationRatio;
            
            // Compute accelerations
            angularAcceleration = Mathf.Clamp(angularAcceleration, -maximumAngularAcceleration, maximumAngularAcceleration);
            linearAcceleration = Mathf.Clamp(linearAcceleration, -maximumLinearDeceleration, maximumLinearAcceleration);
            
            // Force stop if too slow
            if (linearVelocity.sqrMagnitude < stopThreshold * stopThreshold)
            {
                currentLinearVelocity = 0f;
                linearVelocity = Vector2.zero;
            }

            // Velocity update
            currentLinearVelocity += linearAcceleration * Time.deltaTime;
            currentLinearVelocity = Mathf.Clamp(currentLinearVelocity, -maximumLinearVelocity, maximumLinearVelocity);
            linearVelocity = transform.right * currentLinearVelocity;
            
            angularVelocity += angularAcceleration * Time.deltaTime;
            angularVelocity = Mathf.Clamp(angularVelocity, -maximumAngularVelocity, maximumAngularVelocity);
            angularVelocity = angularVelocity * accelerationRatio;
            
            // Position / rotation update
            transform.position += (Vector3) (linearVelocity * Time.deltaTime);
            transform.localEulerAngles += -Vector3.forward * angularVelocity * Time.deltaTime;

            Debug.DrawLine(transform.position, transform.position + (Vector3) linearVelocity* 10, Color.green);
            //Debug.DrawLine(transform.position, transform.position + transform.right * linearAcceleration* 10, Color.yellow);
        }
    }
      
}
