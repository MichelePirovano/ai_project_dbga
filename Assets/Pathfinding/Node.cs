﻿using System;
using UnityEngine;
using Random = System.Random;

namespace AI.Pathfinding
{
	public class Node : MonoBehaviour
	{
		public Color color = Color.white;

		public TextMesh text;
		
		// Used only by the search algorithms
		public float value; 
		
		public void Draw()
		{
			text.text = ((int)value).ToString();
			GetComponent<SpriteRenderer>().color = color;
		}
	}
}