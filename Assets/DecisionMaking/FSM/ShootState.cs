﻿using System.Collections;
using System.Collections.Generic;
using AI.Decision;
using AI.Steering;
using UnityEngine;

public class ShootState : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
    }
    
    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        var myBrain = animator.GetComponentInParent<Brain>();
        var allAgents = GameObject.FindObjectsOfType<Agent>();

        Agent closestAgent = null;
        var minDistance = Mathf.Infinity;
			
        foreach (var otherAgent in allAgents)
        {
            if (otherAgent == myBrain.GetComponent<Agent>()) continue;
            if (otherAgent.GetComponent<HealthState>().team == myBrain.GetComponent<HealthState>().team) continue;

            var dist = Vector2.Distance(otherAgent.transform.position, myBrain.transform.position);
            if (dist < minDistance)
            {
                minDistance = dist;
                closestAgent = otherAgent;
            }
        }

        if (closestAgent != null)
        {
            myBrain.GetComponent<Shooter>().StartShooting();
            var seekBe = myBrain.GetComponent<SeekBehaviour>();
            seekBe.weight = 1;
            seekBe.targetTransform = closestAgent.transform;
            myBrain.GetComponent<Agent>().maximumLinearVelocity = 0;	// Seek, without moving
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

}
