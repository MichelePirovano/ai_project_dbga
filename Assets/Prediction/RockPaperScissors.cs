﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.NGrams
{
	public enum Action
	{
		ROCK = 1,
		PAPER = 2,
		SCISSORS = 3
	}

	public class Answer
	{
		public Action action;
	}

	public class RockPaperScissors : MonoBehaviour
	{
		// Statistics
		private int nGames = 0;
		private int nWon = 0;
		private int nLost = 0;
		private int nDraw = 0;

		public int MaxGames = 10;

		public int LogPeriod = 10;
		
		public Player Player1;
		public Player Player2;
		
		IEnumerator Start()
		{
			while (nGames < MaxGames)
			{
				Answer answer = new Answer();
				yield return Player1.GetAction(answer);
				Action pl1Action = answer.action;

				answer = new Answer();
				yield return Player2.GetAction(answer);
				Action pl2Action =  answer.action;

				bool log = nGames % LogPeriod == 0;
				
				if (log) Debug.Log("Game " + nGames +":"+ pl1Action + " - " + pl2Action);

				Player1.ReceiveOpponentAction(pl2Action);
				Player2.ReceiveOpponentAction(pl1Action);
				
				int diff = ((int) pl1Action - (int) pl2Action);
				if (diff == 1 || diff == -2)
				{
					if (log) Debug.Log("PL1 WINS");
					nWon++;
				}
				else if (diff == 2 || diff == -1)
				{
					if (log) Debug.Log("PL1 LOSES");
					nLost++;
				}
				else
				{
					if (log) Debug.Log("DRAW");
					nDraw++;
				}
				nGames++;
				if (log) yield return null;
			}

			float pl1Winrate = nWon * 100 / (float) nGames;
			float pl2Winrate = nLost * 100 / (float) nGames;
			Debug.LogError("PL1 winrate: " + pl1Winrate.ToString("0.00") + "%"
			+ "   PL2 winrate: " + pl2Winrate.ToString("0.00") + "%"
			+ "\n nGames: " + nGames + " nWon: " + nWon + " nLost: " + nLost + " nDraw: " + nDraw);
		}
		
	}
    
}
