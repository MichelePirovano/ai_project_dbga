﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Decision
{
    public class Shooter : MonoBehaviour
    {
        public GameObject bulletPrefab;

        void Awake()
        {
            //StartShooting();
        }
        
	    public void Shoot ()
        {
            var bulletGo = Instantiate(bulletPrefab);
            bulletGo.transform.position = transform.position + transform.right * 1f;
            bulletGo.transform.rotation = transform.rotation;
	    }


        public float shootRate = 1.0f;

        private bool isShooting = false;
        
        internal void StartShooting()
        {
            if (isShooting) return;
            isShooting = true;
            InvokeRepeating("Shoot", 0f, shootRate);
        }

        internal void StopShooting()
        {
            if (!isShooting) return;
            isShooting = false;
            CancelInvoke("Shoot");
        }
    }

}
