﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.Steering
{
	public class GridFormationBehaviour : SteeringBehaviour
	{
		public Transform targetTransform;
		
		public int width = 3;
		public int height = 3;

		public int myIndex;

		public float formationSpan = 1f;

		public float arrivalThreshold = 1f;
		
		public override SteeringOutput GetSteering()
		{
			SteeringOutput steering = new SteeringOutput();

			var formationPos = (Vector2)targetTransform.position;

			var personalX = myIndex / width;
			var personalY = myIndex % height;

			var personalPos = formationPos + (Vector2.right * personalX + Vector2.up * personalY) * formationSpan;
			
			var direction = (personalPos - (Vector2)transform.position);
			steering.targetLinearVelocityPercent = direction.normalized;

			if (Vector2.SqrMagnitude(personalPos - (Vector2)transform.position) <= arrivalThreshold*arrivalThreshold)
			{
				//steering.inactive = true;
				steering.targetLinearVelocityPercent = Vector2.zero;
			}
			
			return steering;
		}
	}

}