﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AI.TicTacToe
{
	/// <summary>
	/// Choose an action at random
	/// </summary>
	public class RandomChooser : MonoBehaviour, ITicTacToeAI
	{
		public IEnumerator GetBestAction(TicTacToe game, State state, Action outputAction)
		{  
			var possibleActions = game.GetPossibleActions(state);
			foreach (var possibleAction in possibleActions)
			{
				Debug.Log(possibleAction);
			}
			var chosenActionIndex = UnityEngine.Random.Range(0, possibleActions.Count);
			var chosenAction = possibleActions[chosenActionIndex];
			outputAction.i = chosenAction.i;
			outputAction.j = chosenAction.j;
			outputAction.value = chosenAction.value;
			yield return new WaitForSeconds(2f);
		}
	}
}