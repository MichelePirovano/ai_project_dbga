﻿namespace AI.Decision.GOAP
{
	public class Action_CraftAxe : Action
	{
		protected override void Awake()
		{
			base.Awake();
			preconditions["wood"] = 4;		 // >=
			preconditions["durability"] = 0; // ==
			effects["durability"] = +5;
			effects["wood"] = -4;
		}

		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
			agent.axeDurability += effects["durability"];
		}
	}
}