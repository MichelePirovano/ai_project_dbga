﻿namespace AI.Decision.GOAP
{
	public class Action_PickupWood : Action
	{
		protected override void Awake()
		{
			base.Awake();
			effects["wood"] = +3;
		}

		public override void Perform(Agent agent)
		{
			agent.currentWood += effects["wood"];
		}
	}
}