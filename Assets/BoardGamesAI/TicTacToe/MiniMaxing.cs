﻿using System.Collections;
using UnityEngine;

namespace AI.TicTacToe
{
	/// <summary>
	/// Choose an action using the MiniMax algorithm
	/// </summary>
	public class MiniMaxing : MonoBehaviour, ITicTacToeAI
	{
		private VisualBoard visualBoard;
		
		public IEnumerator GetBestAction(TicTacToe game, State state, Action outputAction)
		{
			visualBoard = FindObjectOfType<VisualBoard>();
			while (true)
			{
				if (hasPressed) break;
				yield return null;
			}
			hasPressed = false;

			var stateScore = new StateScore();
			yield return RecursiveMiniMaxing(game, state, outputAction, stateScore);
			Debug.LogError($"For this state, the score is {stateScore.value}");
			
			visualBoard.stateValue.text = stateScore.value.ToString();
			Debug.LogError("Algorithm ended! Best action: " + outputAction);

			// Show the final action:
			state.cells[outputAction.i, outputAction.j] = outputAction.value;
			visualBoard.ShowState(state);
		}

		public class StateScore
		{
			public int value;
		}

		private bool hasPressed = false;
		void Update()
		{
			// Commented to make it automatic
			//if (Input.GetKeyDown(KeyCode.Space))
			{
				hasPressed = true;
			}
		}
		
		
		private IEnumerator RecursiveMiniMaxing(TicTacToe game, State state, Action outputAction, StateScore stateScore)
		{
			visualBoard.ShowState(state);
			Debug.LogError("Showing state for player " + state.player);
			while (true)
			{
				if (hasPressed) break;
				yield return null;
			}
			hasPressed = false;
			
			// If it is a terminal state, we return the score
			CellValue terminalStateWinner;
			if (game.IsTerminalState(state, out terminalStateWinner))
			{
				//Debug.Log("This is a terminal state!");
				if (terminalStateWinner == CellValue.Draw)
				{
					stateScore.value = 0;
				}
				else if (terminalStateWinner == state.player)
				{
					stateScore.value = 10;
				}
				else
				{
					stateScore.value = -10;
				}
				
				Debug.Log("State value is " + stateScore.value);
				visualBoard.stateValue.text = stateScore.value.ToString();
				while (true)
				{
					if (hasPressed) break;
					yield return null;
				}
				hasPressed = false;
				yield break;
			}
			
			// If it is not a terminal state....
			
			// We try all possible actions
			//Debug.Log("Not a terminal state, try actions...");
			var possibleActions = game.GetPossibleActions(state);
			var bestValue = -100000;
			Action bestAction = null;
			foreach (var possibleAction in possibleActions)
			{
				//Debug.Log("Trying action " + possibleAction);
				// For each action, get the Next state
				var s2 = game.StateTransition(state, possibleAction);
				var tmpStateScore = new StateScore();
				
				// Call the recursive algorithm to get the value/score of that state
				yield return RecursiveMiniMaxing(game, s2, outputAction, tmpStateScore);

				// Since the score in s2 is defined from the view of the opponent, invert the score
				tmpStateScore.value *= -1;
				
				// Keep the best action
				var value = tmpStateScore.value;
				if (value > bestValue)
				{
					bestValue = value;
					bestAction = possibleAction;
				}
			}
			
			// We now have the best action!
			outputAction.i = bestAction.i;
			outputAction.j = bestAction.j;
			outputAction.value = bestAction.value;

			// And the score of this state!
			stateScore.value = bestValue;
			visualBoard.stateValue.text = stateScore.value.ToString();
			
			// Pause until you click something
			while (true)
			{
				if (hasPressed) break;
				yield return null;
			}
			hasPressed = false;
		}
		
	}
}