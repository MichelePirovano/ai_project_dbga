﻿using System.Collections.Generic;
using UnityEngine;

namespace AI.Decision.GOAP
{
	public abstract class Action : MonoBehaviour
	{
		public int actionCost = 1;
		
		public Dictionary<string, int> preconditions;
		public Dictionary<string, int> effects;

		protected virtual void Awake()
		{
			preconditions = new Dictionary<string, int>();
			effects = new Dictionary<string, int>();
		}

		public abstract void Perform(Agent agent);
	}
}
