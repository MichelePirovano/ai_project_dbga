﻿using System.Collections;
using System.Collections.Generic;
using AI.Steering;
using UnityEngine;

namespace AI.Decision.FSM
{
	public class FSM_DecisionMaker : DecisionMaker
	{
		public Animator aiAnimator;

		public override void MakeDecision()
		{
			var myBrain = GetComponent<Brain>();
			GetComponent<Shooter>().StopShooting();
			GetComponent<Agent>().maximumLinearVelocity = 5;
			
			// Evaluate the conditions and update the transitions
			var myHealth = GetComponent<HealthState>().health;
			aiAnimator.SetInteger("MyHealth", (int)myHealth);

			var minDistance = 15;
			var allAgents = GameObject.FindObjectsOfType<Agent>();
			bool enemyClose = false;
			foreach (var otherAgent in allAgents)
			{
				if (otherAgent == myBrain.GetComponent<Agent>()) continue;
				if (otherAgent.GetComponent<HealthState>().team == myBrain.GetComponent<HealthState>().team) continue;
				if (Vector2.SqrMagnitude(otherAgent.transform.position - myBrain.transform.position) < minDistance * minDistance)
				{
					enemyClose = true;
					break;
				}
			}
			aiAnimator.SetBool("EnemyClose", enemyClose);
			
			// Execute / Handle the current state
			// (handled automatically by our Animator)
		}

	}

}
